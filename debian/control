Source: liblong-jump-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libimporter-perl <!nocheck>,
                     libtest2-suite-perl (>= 0.000126) <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liblong-jump-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liblong-jump-perl.git
Homepage: https://metacpan.org/release/Long-Jump
Rules-Requires-Root: no

Package: liblong-jump-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libimporter-perl
Description: mechanism for returning to a specific point from a deeply nested stack
 Long::Jump essentially provides a multi-level return. You can mark a spot
 with setjump() and then unwind the stack back to that point from any nested
 stack frame by name using longjump(). You can also provide a list of return
 values.
 .
 This is not quite a match for C's long jump, but it is "close enough". It is
 safer than C's jump in that it only lets you escape frames by going up the
 stack, you cannot jump in other ways.
